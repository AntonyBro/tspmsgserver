package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	session    *mgo.Session
	err        error
	users      *mgo.Collection
	messages   *mgo.Collection
	GCMSendApi = "https://android.googleapis.com/gcm/send"
	client     *Client
)

type User struct {
	Name     string
	Email    string
	Password string
	GcmId    string
}

type SetMessage struct {
	FromEmail string
	ToEmail   string
	Message   string
}

type GetMessage struct {
	Id        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	FromEmail string
	ToEmail   string
	Message   string
}

type Client struct {
	key        string
	httpClient *http.Client
}

func New(key string) *Client {
	return &Client{
		key:        key,
		httpClient: new(http.Client),
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	password := r.URL.Query().Get("password")

	result := User{}
	err = users.Find(bson.M{"email": email}).One(&result)

	if err != nil {
		fmt.Fprintf(w, "login")
	} else {
		if result.Password == password {
			result.Password = ""
			response, _ := json.Marshal(result)
			fmt.Fprintf(w, string(response))
		} else {
			fmt.Fprintf(w, "login")
		}
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")

	result := User{}
	err = users.Find(bson.M{"email": email}).One(&result)
	if err != nil {
		fmt.Fprintf(w, "0")
	} else {
		fmt.Fprintf(w, "1")
	}
}

func registration(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	email := r.URL.Query().Get("email")
	password := r.URL.Query().Get("password")
	gcmId := string("id")

	result := User{}
	err = users.Find(bson.M{"email": email}).One(&result)

	if err != nil {
		user := User{name, email, password, gcmId}
		err = users.Insert(&user)
		user.Password = ""
		response, _ := json.Marshal(user)
		fmt.Fprintf(w, string(response))
	} else {
		fmt.Fprintf(w, "registration")
	}
}

func gcmRegister(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")
	registrationId := r.URL.Query().Get("id")

	result := User{}
	err = users.Find(bson.M{"email": email}).One(&result)

	if err == nil {
		users.Update(bson.M{"email": email}, bson.M{
			"name":     result.Name,
			"email":    result.Email,
			"password": result.Password,
			"gcmid":    registrationId})

		fmt.Fprintf(w, string("1"))
	} else {
		fmt.Fprintf(w, "registration")
	}

	//	var results []User
	//	erre := users.Find(nil).All(&results)
	//	if erre != nil {
	//		panic(erre)
	//	}

	//	fmt.Println("Results All: ", results)
}

func searchUser(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")

	var results []User

	err := users.Find(bson.M{"name": name}).All(&results)
	if err != nil {
		fmt.Fprintf(w, "0")
	} else {
		if results != nil {
			for i, _ := range results {
				results[i].Password = ""
			}
			js, _ := json.Marshal(results)
			fmt.Fprintf(w, string(js))
		} else {
			fmt.Fprintf(w, "0")
		}
	}
}

func setMessage(w http.ResponseWriter, r *http.Request) {
	fromEmail := r.URL.Query().Get("from")
	toEmail := r.URL.Query().Get("to")
	msg := r.URL.Query().Get("msg")

	message := SetMessage{fromEmail, toEmail, msg}

	err := messages.Insert(&message)

	if err != nil {
		fmt.Fprintf(w, "0")
	} else {
		result := User{}
		users.Find(bson.M{"email": toEmail}).One(&result)

		gcmSend("{\"data\":{\"message\":\"" + toEmail + "\"},\"to\":\"" + result.GcmId + "\"}")
		fmt.Fprintf(w, "1")
	}
}

func gcmSend(email string) {
	request, _ := http.NewRequest("POST", GCMSendApi, bytes.NewBufferString(email))

	request.Header.Add("Authorization", fmt.Sprintf("key=%s", client.key))
	request.Header.Add("Content-Type", "application/json")

	resp, _ := client.httpClient.Do(request)
	fmt.Printf(resp.Status)

	defer resp.Body.Close()
}

func getMessage(w http.ResponseWriter, r *http.Request) {
	email := r.URL.Query().Get("email")

	var results []GetMessage

	err := messages.Find(bson.M{"toemail": email}).All(&results)
	if err != nil {
		fmt.Fprintf(w, "0")
	} else {
		if results != nil {
			js, _ := json.Marshal(results)
			fmt.Fprintf(w, string(js))
			for _, element := range results {
				messages.RemoveId(element.Id)
			}
		} else {
			fmt.Fprintf(w, "0")
		}
	}
}

func main() {
	client = New("AIzaSyAUiJcaGXztL570Aq0mwOmVagpU2bx2E9Q")

	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)
	users = session.DB("tsp_messenger_db").C("users")
	messages = session.DB("tsp_messenger_db").C("messages")

	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.HandleFunc("/registration", registration)
	http.HandleFunc("/gcm", gcmRegister)
	http.HandleFunc("/search/user", searchUser)
	http.HandleFunc("/set/message", setMessage)
	http.HandleFunc("/get/message", getMessage)

	port := "9000"
	http.ListenAndServe("128.199.52.224:"+port, nil)
	//	http.ListenAndServe(":"+"8080", nil)

}
